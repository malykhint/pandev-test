FROM maven:3.6.3-openjdk-17-slim as build
COPY . .
RUN mvn clean install -Dmaven.test.skip=true

FROM openjdk:17-jdk-alpine3.14
COPY --from=build target/demo-0.0.1-SNAPSHOT.jar demo-0.0.1-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "demo-0.0.1-SNAPSHOT.jar"]
