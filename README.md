# Схема CI/CD процесса.
![enter image description here](https://i.ibb.co/wMfgvzk/image.jpg)

## Описание  этапов CI/CD процесса.
1. **Написание кода:**
Разработчики пишут код и сохраняют его состояние в Git-репозитории, например в GitLab. 

2. **Тестирование:** 
Весь продукт проверяется на наличие ошибок в виртуальной среде, например при помощи метода Shell в Gitlab-runner.
> **Gitlab-runner** — это программа-агент, которая выполняет инструкции указанные в файлах .gitlab-ci.yml

3. **Сборка:**
Если на этапе тестирования не было ошибок, то весь продукт запаковывается в Image, например при помощи метода Dind в Gitlab-runner. Затем полученный Image загружается в хранилище контейнеров, например в Nexus.
> **Dind** - Docker-in-Docker представляет собой виртуализированную среду Docker, запущенную в самом контейнере для сборки образов контейнера

4. **Развертывание:**
Из хранилища контейнеров Gitlab-runner берет Image, содержащий актуальный программный релиз, и разворачивает его на серверах, например при помощи Docker compose.

5. **Поддержка и отслеживание:**
После развертывания приложение становится доступным конечным пользователям. Параллельно этому разработчики выполняют его поддержку и анализируют опыт взаимодействия пользователей с продуктом.

6. **Планирование разработок новых версий и патчей:**
Готовится план доработок на основе опыта взаимодействия пользователей с программой, разработчиками вносятся необходимые изменения в программу. Далее весь процесс запускается заново.

## Интеграция с Telegram.
Механизм отправки уведомлений в Telegram бот при помощи bash-скрипта  **.ci-notify.sh**, где **TELEGRAM_BOT_TOKEN** - токен, присланный после создания бота через BotFather, а **TELEGRAM_USER_ID** - ID пользователя, которому будут отправляться уведомления:

```bash
#!/bin/bash 
TIME="10" 
URL="https://api.telegram.org/bot$TELEGRAM_BOT_TOKEN/sendMessage" 
TEXT="Deploy status: $1%0A%0AProject:+$CI_PROJECT_NAME%0AURL:+$CI_PROJECT_URL/pipelines/$CI_PIPELINE_ID/%0ABranch:+$CI_COMMIT_REF_SLUG" 
curl -s --max-time $TIME -d "chat_id=$TELEGRAM_USER_ID&disable_web_page_preview=1&text=$TEXT"  $URL > /dev/null
```
Также необходимо в **.gitlab-ci.yml** добавить команду и условие запуска скрипта, например:
```yml
deploy:
 stage: deploy
 script:
	 - sh .ci-notify.sh  ✅
notify_error:
 stage: notify
 script:
	 - sh .ci-notify.sh  ❌
 when:  on_failure
```
Результат:

![enter image description here](https://i.ibb.co/b6RFTVW/2023-11-22-14-48-37.png)
# Практическая часть 
  
[![pipeline status](https://gitlab.com/malykhint/pandev-test/badges/main/pipeline.svg)](https://gitlab.com/malykhint/pandev-test/-/commits/main)

В качестве примера:

 1. на виртуальную машину установлен **Docker**, **Docker compose** и **Gitlab-runner**(shell и dind)
 2. сгенерировано простое **Java Spring Boot** приложение, создающее веб-страницу приветствия, при помощи сервиса https://start.spring.io/
 ```java
package com.example.demo;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@SpringBootApplication
@RestController
public class DemoApplication {
    public static void main(String[] args) {
    SpringApplication.run(DemoApplication.class, args);
}
    @GetMapping("/")
    public String hello(@RequestParam(value = "name", defaultValue = "PanDev") String name) {
    return String.format("Hello, %s!", name);
    }
   }
```
3. написан **Dockerfile** для сборки приложения в multistage режиме 
```docker
FROM maven:3.6.3-openjdk-17-slim as build
COPY . .
RUN mvn clean install -Dmaven.test.skip=true
FROM openjdk:17-jdk-alpine3.14
COPY --from=build target/demo-0.0.1-SNAPSHOT.jar demo-0.0.1-SNAPSHOT.jar
EXPOSE  8080
ENTRYPOINT ["java", "-jar", "demo-0.0.1-SNAPSHOT.jar"]
```
4. написан **docker-compose.yml**, запускающий 2 экземпляра приложения (из собранного через Dockerfile образа) и 1 nginx в качестве балансировщика
```yml
version: "3"

services:
  app1:
    image: registry.gitlab.com/malykhint/pandev-test/pandev-test:latest
    container_name: demoapp1
    restart: unless-stopped
    networks:
      - backend
  app2:
    image: registry.gitlab.com/malykhint/pandev-test/pandev-test:latest
    container_name: demoapp2
    restart: unless-stopped
    networks:
      - backend
  balancer:
    image: nginx:alpine3.18
    container_name: balancer-demoapp
    restart: unless-stopped
    networks:
      - backend
      - frontend
    ports:
      - "80:80"
    volumes:
      - ./nginx/demoapp.conf:/etc/nginx/conf.d/default.conf

networks:
  backend:
    name: backend
    external: false
  frontend:
    name: frontend

```
5. подготовлен **.gitlab-ci.yml** для воссоздания CI/CD процесса, производящий тестирование, сборку, развертывание, а также нотификацию по статусу деплоя в телеграм
```yml
stages:
    - test
    - build
    - deploy
    - notify

test:
    stage: test
    script:
        - mvn test -Dtest=com.example.demo.DemoApplicationTests
    tags:
        - test-shell

docker build:
    stage: build
    image: docker:stable
    services:
        - docker:dind
    script:
        - docker login -u gitlab-ci-token -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
        - docker build -t ${CI_REGISTRY}/${CI_PROJECT_PATH}/${CI_PROJECT_NAME}:${CI_COMMIT_REF_SLUG} .
        - docker push ${CI_REGISTRY}/${CI_PROJECT_PATH}/${CI_PROJECT_NAME}:${CI_COMMIT_REF_SLUG}
        - docker tag ${CI_REGISTRY}/${CI_PROJECT_PATH}/${CI_PROJECT_NAME}:${CI_COMMIT_REF_SLUG}
                   ${CI_REGISTRY}/${CI_PROJECT_PATH}/${CI_PROJECT_NAME}:latest
        - docker push ${CI_REGISTRY}/${CI_PROJECT_PATH}/${CI_PROJECT_NAME}:latest
    tags:
        - dev-docker

deploy to Dev:
    stage: deploy
    script:
        - docker login -u gitlab-ci-token -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
        - docker rm -f $(docker ps -q) || true
        - docker pull ${CI_REGISTRY}/${CI_PROJECT_PATH}/${CI_PROJECT_NAME}:latest
        - docker compose up -d
        - sh .ci-notify.sh ✅
    tags:
        - dev-shell

notify error:
    stage: notify
    script:
        - sh .ci-notify.sh ❌
    when: on_failure
```
## Итог
Обратимся к запущенному приложению:
```bash
input: curl -s http://ip
output: Hello, PanDev!
```